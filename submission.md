####  My name is George McAskill

This is my submission for homework1. 
I would like to include one link: [name your link](url of your link)

I would also like to include some facts in a table:

|   Item        |          Info            |
|---------------|--------------------------|
| home town     | Marietta, GA 		 |
| favorite song | Slide Away - Oasis, Lateralus - Tool,  	 |
| <anything else> | <that you want to share with the class>
